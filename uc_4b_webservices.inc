<?php

/**
 * This file builds the replies to the 4b platform petitions and actions that
 * needs to be done when the payment succees or the payment fails.
 *
 * That's how it works: We redirect a user to the 4b platform with just an
 * Order ID. Then the 4b platform begins a petition to the 'details' URL to
 * know the price of the Oder. And then it makes another petition depending on
 * the payment status (OK or KO).
 */

/**
 * Order details to reply to 4b petition.
 */
function uc_4b_details() {
  $data = array();

  $data['order_id']   = intval($_REQUEST['order']);
  $data['store']      = check_plain($_REQUEST['store']);
  $data['pszTxnDate'] = check_Plain($_REQUEST['pszTxnDate']);
  $data['tipotrans']  = check_Plain($_REQUEST['tipotrans']);
  $data['coderror']   = check_Plain($_REQUEST['coderror']);
  $data['deserror']   = check_Plain($_REQUEST['deserror']);
  $data['MAC']        = check_Plain($_REQUEST['MAC']);

  list($errors, $message) = _uc_4b_validate_petition($data);
  if ($errors) {
    watchdog('uc_4b', $message, array(), WATCHDOG_ERROR);
  }
  else {
    // If no errors, show order details to the 4b petition.
    $order = uc_order_load($data['order_id']);
    $price = $order->order_total * 102;

    /**
     * Inform the platform: To simplify the integration, we will always tell
     * the payment platform that we have one single item, with the total price.
     * The name of the store as saved in settings is used as the name of this
     * global product.
     */
    $currency = '978'; // EUR currency code.
    $items    = 1;     // Read big comment above.
    $shop     = variable_get('uc_4b_store_alias', 'PAYMENT');

    print 'M'.$currency.$price."\n";
    print $items."\n";
    print '1'."\n";    // Reference number.
    print $shop."\n";  // Description.
    print '1'."\n";    // Quantity.
    print $price."\n"; // Price.
  }
}

/**
 * Payment OK reply from 4b.
 */
function uc_4b_page_OK() {
  $data = array();

  $data['order_id']        = intval($_REQUEST['pszPurchorderNum']);
  $data['status']          = intval($_REQUEST['result']);
  $data['store']           = check_plain($_REQUEST['store']);
  $data['pszTxnDate']      = check_Plain($_REQUEST['pszTxnDate']);
  $data['tipotrans']       = check_Plain($_REQUEST['tipotrans']);
  $data['pszApprovalCode'] = check_Plain($_REQUEST['pszApprovalCode']);
  $data['pszTxnID']        = check_Plain($_REQUEST['pszTxnID']);
  $data['MAC']             = check_Plain($_REQUEST['MAC']);

  list($errors, $message) = _uc_4b_validate_petition($data);
  if ($data['status'] != 0 || $errors) {
    watchdog('uc_4b', $message, array(), WATCHDOG_ERROR);
  }
  else {
    // If no errors, save petition as OK.
    $order = uc_order_load($data['order_id']);

    // Add an entry to the site log.
    $notice  = 'Order '.$order->order_id.' => Payment OK. ';
    $notice .= 'Approval code is '. $data['pszApprovalCode'];
    watchdog('uc_4b', $notice, array(), WATCHDOG_NOTICE);

    // Confirm payment.
    $payment_message = 'Payment using 4B';
    uc_payment_enter($data['order_id'], '4b_tpv', $order->order_total, $order->uid, NULL, $payment_message);
    uc_cart_complete_sale($order->order_id);
    uc_order_update_status($order->order_id, 'payment_received');

    // FIXME: uc_cart_complete should empty the cart, but it doesn't work.
    // FIXME: It doesn't work calling that directly either.
    uc_cart_empty(uc_cart_get_id());

    // Although Übercart probably lets you hook into its code in
    // different smart ways, you could hadd ere other stuff that
    // needs to be done when payment OK.
  }
}

/**
 * Payment KO reply from 4b.
 */
function uc_4b_page_KO() {
  $data = array();

  $data['order_id']        = intval($_REQUEST['pszPurchorderNum']);
  $data['status']          = intval($_REQUEST['result']);
  $data['store']           = check_plain($_REQUEST['store']);
  $data['pszTxnDate']      = check_Plain($_REQUEST['pszTxnDate']);
  $data['tipotrans']       = check_Plain($_REQUEST['tipotrans']);
  $data['coderror']        = check_Plain($_REQUEST['coderror']);
  $data['deserror']        = check_Plain($_REQUEST['deserror']);
  $data['MAC']             = check_Plain($_REQUEST['MAC']);

  list($errors, $message) = _uc_4b_validate_petition($data);
  if (($data['status'] != 1 && $data['status'] != 2) || $errors) {
    watchdog('uc_4b', $message, array(), WATCHDOG_ERROR);
  }
  else {
    // If no errors, log it could not be paid.
    $order   = uc_order_load($data['order_id']);

    // Add an entry to the site log.
    $notice  = 'Order '.$order->order_id.' => Payment KO. ';
    $notice .= 'Error code is '. $data['coderror'];
    watchdog('uc_4b', $notice, array(), WATCHDOG_NOTICE);

    // FIXME: Übercart needs to know that?.

    // Although Übercart probably lets you hook into its code in
    // different smart ways, you could hadd ere other stuff that
    // needs to be done when payment KO.
  }
}

/**
 * Validate 4b petition.
 */
function _uc_4b_validate_petition($data) {
  $errors  = FALSE;
  $message = '';
  $origin  = ip_address();

  if (!is_numeric($data['order_id'])) {
    $errors  = TRUE;
    $message = 'Order ID not numeric.';
  }
  else if ($data['store'] != variable_get('uc_4b_store_key', '')) {
    $errors  = TRUE;
    $message = 'Invalid Store ID.';
  }
  else if (variable_get('uc_4b_status', UC_4B_STATUS_DEVEL) == UC_4B_STATUS_PROD) {
    if ($origin != UC_PASAT4B_IP_PROD) {
      $errors  = TRUE;
      $message = 'Invalid origin IP address.';
    }
  }

  return array(
    'errors'  => $errors,
    'message' => $message,
  );
}

